<?php 

$jonsdata = file_get_contents("json/data.json");
$json = json_decode($jonsdata,true);
?> 


<div class="container  content-right ">
   <div class="skill">
      <i class="fa fa-suitcase icon-color" aria-hidden="true"></i>
      <span class="text-content"><?php echo $json['heading-top'] ?></span>
   </div>
   <?php foreach ($json['Work_Experience'] as $key): ?>
   
   <div class="main">
      <div class="text-content">
         <span><?php echo $key['title'] ?></span>
      </div>
      <div class="icon-text">
         <i class="fa fa-calendar icon-color icon" aria-hidden="true"></i>
         <div class="date color-text-icon"><span><?php echo $key['fromdate'] ?>
          </span> -
          <?php if($key['todate'] != "Current"): ?>
           
            <span><?php echo $key['todate'] ?>
          </span>
        </div>
         
         <?php else: ?>
          <span class="bg-common bg-blue"><?php echo $json['current'] ?></span></div>
         
         
         <?php endif; ?>
      </div>
      <p class="text-ct"><?php echo $key['content'] ?>
      </p>
   </div>
  
   <div class="hr-right"></div>
   <?php endforeach; ?>
   <!-- <div class="main">
      <div class="text-content">
         <span>Web Developer / something.com</span>
      </div>
      <div class="icon-text">
         <i class="fa fa-calendar icon-color icon" aria-hidden="true"></i>
         <div class="date color-text-icon">Mar 2012 - Dec 2014</div>
      </div>
      <p class="text-ct">Consectetur adipisicing elit. Praesentium magnam consectetur
         vel in deserunt aspernatur est reprehenderit sunt hic. Nulla
         tempora soluta ea et odio, unde doloremdque repellendus iure,
         iste.
      </p>
   </div>
   <div class="hr-right"></div>
   <div class="main">
      <div class="text-content">
         <span>Graphic Designer / designsomething.com</span>
      </div>
      <div class="icon-text">
         <i class="fa fa-calendar icon-color icon" aria-hidden="true"></i>
         <div class="date color-text-icon">Jun 2010 - Mar 2012</div>
      </div>
      <p class="text-ct">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      </p>
   </div> -->
</div>
<!-- sesion education -->
<div class="container  content-right  content-space">
   <div class="skill">
      <i class="fa fa-certificate icon-color" aria-hidden="true"></i>
      <span class="text-content"><?php echo $json['heading-bottom'] ?></span>
   </div>
   <?php foreach ($json['Education'] as $key): ?>
   
   <div class="main">
      <div class="text-content">
         <span><?php echo $key['title'] ?></span>
      </div>
      <div class="icon-text">
         <i class="fa fa-calendar icon-color icon" aria-hidden="true"></i>
         <span class="date color-text-icon"><?php echo $key['date'] ?></span>
      </div>
      <p class="text-ct"><?php echo $key['content'] ?>
      </p>
   </div>
   <div class="hr-right"></div>
   <?php endforeach ?>
   <!-- <div class="main">
      <div class="text-content">
         <span>LonDon Business School</span>
      </div>
      <div class="icon-text">
         <i class="fa fa-calendar icon-color icon" aria-hidden="true"></i>
         <span class="date color-text-icon">2013 - 2015</span>
      </div>
      <p class="text-ct">Master Degree
      </p>
   </div>
   <div class="hr-right"></div>
   <div class="main">
      <div class="text-content">
         <span>School of Coding</span>
      </div>
      <div class="icon-text">
         <i class="fa fa-calendar icon-color icon" aria-hidden="true"></i>
         <span class="date color-text-icon">2010 - 2013</span>
      </div>
      <p class="text-ct">Bachelor Degree
      </p>
   </div> -->
</div>
</div>
<footer class="bg-common clearn">
   <div class="item-ft">
      <h3 class="item-ft-title"><?php echo $json['title-ft'] ?> </h3>
   </div>
   <div class="item-ft hide">
      <i class="<?php echo $json['icon1'] ?> icon-ft" aria-hidden="true"></i>
      <i class="<?php echo $json['icon2'] ?> icon-ft" aria-hidden="true"></i>
      <i class="<?php echo $json['icon3'] ?> icon-ft" aria-hidden="true"></i>
      <i class="<?php echo $json['icon4'] ?> icon-ft" aria-hidden="true"></i>
      <i class="<?php echo $json['icon5'] ?> icon-ft" aria-hidden="true"></i>
      <i class="<?php echo $json['icon6'] ?> icon-ft" aria-hidden="true"></i>
   </div>
   <div class="item-ft hide">
      <h3 class="item-ft-title">Powered by <u>w3.css</u></h3>
   </div>
</footer>