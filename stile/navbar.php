
<?php 

  $jonsdata = file_get_contents("json/data.json");
  $json = json_decode($jonsdata,true);
  // echo "<pre>";
  // print_r($json['Work_Experience']['heading']);
  
?>
<div class="container-fuild">
<div class="container content-left">
   <div class="avatar">
      <h1><?php echo $json['name']  ?></h1>
   </div>
   <!-- icon and text -->
   <div class="content">
      <i class="fa fa-briefcase icon-color" aria-hidden="true"></i>
      <span class="text-content"><?php echo $json['Designer']  ?></span>
   </div>
   <div class="content">
      <i class="fa fa-home icon-color" aria-hidden="true"></i>
      <span class="text-content"><?php echo $json['address']  ?></span>
   </div>
   <div class="content">
      <i class="fa fa-envelope icon-color" aria-hidden="true"></i>
      <span class="text-content"><?php echo $json['email']  ?></span>
   </div>
   <div class="content">
      <i class="fa fa-phone icon-color" aria-hidden="true"></i>
      <span class="text-content"><?php echo $json['phone']  ?></span>
   </div>
   <!--end icon and text -->
   <!--start skill -->
   <div class="space"></div>
   <div class="skill">
      <i class="fa fa-asterisk icon-color" aria-hidden="true"></i>
      <span class="text-content"><?php echo $json['skill']  ?></span>
   </div>
   <div class="mr-main">
      <div class="text-content fw-content">
         <span><?php echo $json['Photoshop']  ?></span>
      </div>
      <div class="item-skill ">
         <div class="sub-item bg-common" style="width:<?php echo $json['experiencePTS']  ?>%">
            <span class="percent"><?php echo $json['experiencePTS']  ?>%</span>
         </div>
      </div>
   </div>
   <div class="mr-main">
      <div class="text-content fw-content">
         <span><?php echo $json['Photography']  ?></span>
      </div>
      <div class="item-skill ">
         <div class="sub-item bg-common" style="width:<?php echo $json['experiencePTG']  ?>%">
            <span class="percent"><?php echo $json['experiencePTG']  ?>%</span>
         </div>
      </div>
   </div>
   <div class="mr-main">
      <div class="text-content fw-content">
         <span><?php echo $json['Illustration']  ?></span>
      </div>
      <div class="item-skill ">
         <div class="sub-item bg-common" style="width:<?php echo $json['experienceILL']  ?>%">
            <span class="percent"><?php echo $json['experienceILL']  ?>%</span>
         </div>
      </div>
   </div>
   <div class="mr-main">
      <div class="text-content fw-content">
         <span><?php echo $json['Media']  ?></span>
      </div>
      <div class="item-skill">
         <div class="sub-item bg-common" style="width:<?php echo $json['experienceMDA']  ?>%">
            <span class="percent"><?php echo $json['experienceMDA']  ?>%</span>
         </div>
      </div>
   </div>
   <!-- languages -->
   <div class="skill">
      <i class="fa fa-globe icon-color" aria-hidden="true"></i>
      <span class="text-content">Languages</span>
   </div>
   <div class="mr-main">
      <div class="text-content">
         <span>English</span>
      </div>
      <div class="item-skill ">
         <div class="sub-item bg-common" style="width:<?php echo $json['English_experience']  ?>%">
            <span class="percent"></span>
         </div>
      </div>
   </div>
   <div class="mr-main">
      <div class="text-content">
         <span>Spanish</span>
      </div>
      <div class="item-skill ">
         <div class="sub-item bg-common" style="width:<?php echo $json['Spanish_experience']  ?>%">
            <span class="percent"></span>
         </div>
      </div>
   </div>
   <div class="mr-main">
      <div class="text-content">
         <span>German</span>
      </div>
      <div class="item-skill ">
         <div class="sub-item bg-common" style="width:<?php echo $json['German_experience']  ?>%">
            <span class="percent"></span>
         </div>
      </div>
   </div>
</div>

